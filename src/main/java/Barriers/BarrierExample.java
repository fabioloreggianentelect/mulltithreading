package Barriers;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

public class BarrierExample {
    public static final int NUM_THREADS = 4;

    public static void main(String[] args) {
        BarrierSolution barrierSolution = new BarrierSolution();
        barrierSolution.run();
    }
}

class BarrierSolution {

    final CyclicBarrier cyclicBarrier = new CyclicBarrier(BarrierExample.NUM_THREADS);

    public void run() {
        for (int i = 0; i < BarrierExample.NUM_THREADS; i++) {
            Thread thread = new Thread(() -> {
                try {
                    doStuff();
                    cyclicBarrier.await();
                    System.out.println(Thread.currentThread().getName() + " after barrier");
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println();
            });
            thread.start();
        }
    }

    public void doStuff() {
        try {
            int wait = ThreadLocalRandom.current().nextInt(1, 5);
            String message = String.format("%s - waiting for %d seconds", Thread.currentThread().getName(), wait);
            System.out.println(message);
            sleep(wait * 1000);
            System.out.println(Thread.currentThread().getName() + " wait complete. Waiting at barrier now");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
