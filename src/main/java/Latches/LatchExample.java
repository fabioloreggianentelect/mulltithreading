package Latches;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

public class LatchExample {
    public static final int NUM_THREADS = 4;

    public static void main(String[] args) {
        LatchSolution latchSolution = new LatchSolution();
        latchSolution.run();
    }
}

class LatchSolution {

    final CountDownLatch countDownLatch = new CountDownLatch(LatchExample.NUM_THREADS);

    public void run() {
        for (int i = 0; i < LatchExample.NUM_THREADS; i++) {
            Thread thread = new Thread(this::doStuff);
            thread.start();
        }

        System.out.println("Reached latch");
        try {
            countDownLatch.await();
            System.out.println("Went through latch");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void doStuff() {
        try {
            int wait = ThreadLocalRandom.current().nextInt(1, 5);
            String message = String.format("%s - waiting for %d seconds", Thread.currentThread().getName(), wait);
            System.out.println(message);
            sleep(wait * 1000);
            System.out.println(Thread.currentThread().getName() + " wait complete. Latch Countdown");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // NB VERY IMPORTANT
            countDownLatch.countDown();
        }
    }
}
