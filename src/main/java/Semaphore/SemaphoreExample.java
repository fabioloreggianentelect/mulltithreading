package Semaphore;

import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;


public class SemaphoreExample {
    public static final int PERMITS = 2;
    public static final int NUM_THREADS = 4;

    public static void main(String[] args) {
        SemaphoreSolution semaphoreSolution = new SemaphoreSolution();
        semaphoreSolution.run();
    }
}

class SemaphoreSolution {
    final Semaphore throtle = new Semaphore(SemaphoreExample.PERMITS);

    public void run() {
        for (int i = 0; i < SemaphoreExample.NUM_THREADS; i++) {
            Thread thread = new Thread(() -> {
                try {
                    throtle.acquire();
                    doStuff();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    throtle.release();
                }
            });
            thread.start();
        }
    }

    public void doStuff() {
        try {
            String message = String.format("%s - acquired()", Thread.currentThread().getName());
            System.out.println(message);
            int wait = ThreadLocalRandom.current().nextInt(1, 5);
            message = String.format("%s - waiting for %d seconds", Thread.currentThread().getName(), wait);
            System.out.println(message);
            sleep(wait * 1000);
            System.out.println(Thread.currentThread().getName() + " wait complete. Released()");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
