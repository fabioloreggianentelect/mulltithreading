package ThreadPool;

import java.util.ArrayList;
import java.util.List;

public class NoThreadPoolExample {
    static final int MAX_T = 3;

    public static void main(String[] args) throws InterruptedException {
        Runnable r1 = new Task("Task 1 - No Thread Pool");
        Runnable r2 = new Task("Task 2 - No Thread Pool");
        Runnable r3 = new Task("Task 3 - No Thread Pool");
        Runnable r4 = new Task("Task 4 - No Thread Pool");
        Runnable r5 = new Task("Task 5 - No Thread Pool");
        Runnable r6 = new Task("Task 6 - No Thread Pool");
        Runnable r7 = new Task("Task 7 - No Thread Pool");
        Runnable r8 = new Task("Task 8 - No Thread Pool");
        Runnable r9 = new Task("Task 9 - No Thread Pool");

        List<Runnable> runnables = new ArrayList<Runnable>();
        runnables.add(r1);
        runnables.add(r2);
        runnables.add(r3);
        runnables.add(r4);
        runnables.add(r5);
        runnables.add(r6);
        runnables.add(r7);
        runnables.add(r8);
        runnables.add(r9);

        System.out.println("Running no Thread Pool");
        System.out.println("=========================");
        for (int i = 0; i < 9; i += MAX_T) {
            Thread t1 = new Thread(runnables.get(i));
            Thread t2 = new Thread(runnables.get(i + 1));
            Thread t3 = new Thread(runnables.get(i + 2));

            t1.start();
            t2.start();
            t3.start();

            t1.join();
            t2.join();
            t3.join();
        }
    }
}

