package ThreadPool;

import java.text.SimpleDateFormat;
import java.util.Date;

class Task implements Runnable {
    private final String name;

    public Task(String s) {
        name = s;
    }

    public void run() {
        try {
            for (int i = 0; i <= 5; i++) {
                Date d = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
                if (i == 0) {
                    System.out.printf("[%s] Initialization Time for task name - %s = %s%n", Thread.currentThread().getName(), name, ft.format(d));
                    //prints the initialization time for every task
                } else {
                    System.out.printf("[%s] Executing Time for task name - %s = %s%n", Thread.currentThread().getName(), name, ft.format(d));
                    // prints the execution time for every task
                }
                Thread.sleep(1000);
            }
            System.out.println(name + " complete");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
