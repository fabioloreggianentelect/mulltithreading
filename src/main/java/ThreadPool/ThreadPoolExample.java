package ThreadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolExample {
    static final int MAX_T = 3;

    public static void main(String[] args) throws InterruptedException {
        Runnable r1 = new Task("Task 1 - Thread Pool");
        Runnable r2 = new Task("Task 2 - Thread Pool");
        Runnable r3 = new Task("Task 3 - Thread Pool");
        Runnable r4 = new Task("Task 4 - Thread Pool");
        Runnable r5 = new Task("Task 5 - Thread Pool");
        Runnable r6 = new Task("Task 6 - Thread Pool");
        Runnable r7 = new Task("Task 7 - Thread Pool");
        Runnable r8 = new Task("Task 8 - Thread Pool");
        Runnable r9 = new Task("Task 9 - Thread Pool");

        ExecutorService pool = Executors.newFixedThreadPool(MAX_T);

        System.out.println("\nRunning Thread Pool");
        System.out.println("=========================");
        pool.execute(r1);
        pool.execute(r2);
        pool.execute(r3);
        pool.execute(r4);
        pool.execute(r5);
        pool.execute(r6);
        pool.execute(r7);
        pool.execute(r8);
        pool.execute(r9);

        pool.shutdown();
    }
}

