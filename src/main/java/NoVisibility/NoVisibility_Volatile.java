package NoVisibility;

public class NoVisibility_Volatile {
    private static volatile boolean ready;
    private static volatile int amount;

    private static class RenderThread extends Thread {
        @Override
        public void run() {
            while (!ready) {
                Thread.yield();
            }
            System.out.println(amount);
        }
    }

    public static void main(String[] args) {
        new RenderThread().start();
        amount = 42;
        ready = true;
    }
}
